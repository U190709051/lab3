import java.util.Random;
import java.util.Scanner;

public class GuessNumber {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        Random random = new Random();
        int thatNumber = random.nextInt(100);
        boolean guess = false;
        int a;
        System.out.println("Hi! I'm thinking of a number between 0 and 99.");
        System.out.println("Can you guess it : ");
        a = input.nextInt();

        int hmt;
        hmt = 0;

        while(guess==false){


            if (a==thatNumber) {
                System.out.println("Congratulations! you won after "+hmt+" attemps!");
                guess = true;

            }else if(a==-1){
                guess=true;
                System.out.println("sorry the number was "+thatNumber);
            }else{

                System.out.println("Sorry!");
                if(thatNumber>a){
                    System.out.println("Mine is greater than your guess.");
                }else{
                    System.out.println("Mine is less than your guess.");
                }
                System.out.println("Type -1 to quit or guess another: ");
                a = input.nextInt();
                hmt++;
            }
        }
        input.close();

    }
}

